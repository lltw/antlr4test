import org.antlr.v4.runtime.misc.ParseCancellationException;
import pl.intelliseq.antlr.*;

/**
 * Created by ltw on 22.08.16.
 */

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import pl.intelliseq.antlr.*;

import java.io.IOException;

public class main {


    public static void main(String[] args) {

        String sentence = "a cup of tee";

        ANTLRInputStream input = new ANTLRInputStream(sentence);
        DrinkLexer lexer = new DrinkLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        DrinkParser parser = new DrinkParser(tokens);

        DrinkParser.DrinkSentenceContext drinkSentenceContext = parser.drinkSentence();

        ParseTreeWalker walker = new ParseTreeWalker();
        AntlrDrinkListener listener = new AntlrDrinkListener();
        walker.walk(listener, drinkSentenceContext);


        ParseTree drink = parser.drink();
        System.out.println(drink.toStringTree(parser));


    }


}
